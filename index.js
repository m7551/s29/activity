/*
	Firt, we load the express.js module into our application and save it in a variable called express	
*/
const express = require('express');
/*
	Create an application with express.js
	This creates an application that uses express and stores in a varible called app.
	Which makes it easier to use express.js methods in our API
*/
const app = express();
// port is just a variable to contain the port number we want to designate for our new express API
const port = 4000;

/*
	To create a new route in express.js, we first access from our express() package, our method. For GET method request, access express (app), and use get()

	Syntax:
		app.routemethod('/', (req, res) => {
	
			function to handle request and response
		})
*/

// app.use(express.json()) allows your app to read json data
app.use(express.json());

app.get('/', (req, res) => {

	//res.send() ends the response and sends your response to the client
	//res.send() already does the res.writeHead() and res.end() automatically
	res.send("Hello world from our New ExpressJS API!");
})

	//Mini Activity:

// Create a GET route in expressJS which will be able to send a message. Endpoint: /hello
app.get('/hello', (req, res) => {
	// Message: Hello, batch 157!
	res.send("Hello, batch 157!");
})

//Mini Activity #2:

// Change the message that we send. Message: "Hello, I am <name>, I am <age>. I live in <address>"
app.post('/hello', (req, res)=> {
	const {name, age, address} = req.body
	res.send(`Hello, I am ${name}, I am ${age}. I live in ${address}`)
})

let users = [];

app.post('/signup', (req,res)=> {
	console.log(req.body)

	if(req.body.username != '' && req.body.password != ''){
		users.push(req.body);

			console.log(users)
		res.send(`User ${req.body.username} successfully registered.`);
	} else {
		res.send("Please input BOTH username and password.");
	}
});

app.put('/change-password', (req,res)=> {
	let message;

	for (let i = 0; i<users.length; i++){

		if(req.body.username === users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`;
			break;
		} else {
			message = `User does not exist.`;
		}
	}



	res.send(message);
})

// ---------------- ACTIVITY -------------------------

//Create a GET route that will access the "/home" route that will print out a simple message.
app.get('/home', (req, res)=> {
	res.send("Welcome to the home page.");
})

//Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get('/users', (req, res)=> {
	res.send(users);
})

//Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete('/delete-user', (req, res)=> {
	let message;
	const {username, password} = req.body;
	for (i = 0; i < users.length; i++){
		if (username === users[i].username){
			users.splice(i,1) 
			message = `${username} has been deleted.`;
			break;
		} else {
			message = 'User does not exist';
		}
	}
	res.send(message);
})

/*
	app.listen() allows us to designate the correct port to our new express.js API and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running
*/
app.listen(port, () => console.log(`Server is running at port: ${port}.`));